# colorflow

## Run

```shell
zig run src/main.zig -- -color '#ababab' -mode darken -shades
```

**Run with reference traces**

```shell
zig run src/main.zig -freference-trace -- -color '#ababab' -mode darken -shades 6
```
