const std = @import("std");
const colorflow = @import("./Colorflow.zig");

// Cli struct to hold parsed arguments
pub const Cli = struct {
    color: []const u8 = null,
    mode: colorflow.ColorflowMixMode = null,
    shades: u8 = null,
};

// Parse args
pub fn parseArgs(passed_args: [][*:0]const u8) !Cli {
    var result: Cli = undefined;
    // Allocate slice for args
    var args: [][]const u8 = try std.heap.page_allocator.alloc([]const u8, passed_args.len);
    // Iterate of args and copy them to slice
    for (passed_args, 0..) |arg, i| {
        args[i] = std.mem.sliceTo(arg, 0);
    }

    // Loop over agrs
    var i: usize = 1; // start after the program name
    while (i < args.len) : (i += 1) {
        // Match on color and extract color
        if (std.mem.eql(u8, args[i], "-color")) {
            if (i + 1 >= args.len) return error.InvalidArgument;
            result.color = args[i + 1];
            i += 1;
        // Match on mode and parse mode to enum value
        } else if (std.mem.eql(u8, args[i], "-mode")) {
            if (i + 1 >= args.len) return error.InvalidArgument;
            result.mode = colorflow.ColorflowMixMode.fromString(args[i + 1]);
            i += 1;
        // Match on shades and parse shade count
        } else if (std.mem.eql(u8, args[i], "-shades")) {
            if (i + 1 >= args.len) return error.InvalidArgument;
            result.shades = std.fmt.parseInt(u8, args[i + 1], 10) catch {
                return error.InvalidArgument;
            };
            i += 1;
        }
        else {
            // Return error on invalid argument
            return error.InvalidArgument;
        }
    }

    // Return struct
    return result;
}
