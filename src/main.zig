const std = @import("std");
const colorflow = @import("./Colorflow.zig");
const cli = @import("./Cli.zig");
const VERSION = "0.1.0";

// Escape sequence for rgb formatting
const PREFIX_APPLY_HEX = "\x1b[38;2;";

// Show help for using colorflow
fn print_help() void {
    std.debug.print("colorflow - version {s}\n", .{VERSION});
    std.debug.print("Usage: colorflow -color <#hex color> -mode <mode>\n\n", .{});
    std.debug.print("\tValid modes are:\n", .{});
    std.debug.print("\t\tdarken\n", .{});
    std.debug.print("\t\tlighten\n", .{});
    std.debug.print("\t\tmix\n", .{});
}

// Main function
pub fn main() !void {
	const args = std.os.argv;
	// Get struct of parsed args or fail otherwise
    const cli_args = cli.parseArgs(args) catch {
        std.debug.print("[ERROR] Error parsing arguments: {s}\n", .{args[1..]});
        print_help();
        return;
    };

	const str = cli_args.color;
	// Try to parse supplied color or fail otherwise
	const base_color = colorflow.Color.parse(str) catch |err| switch (err) {
    	colorflow.ParseErr.ParseErrorInvalidLength => {
            std.debug.print("[ERROR] Error when parsing content '{s}'. Color code has invalid length\n", .{str});
            return;
    	},
        colorflow.ParseErr.ParseErrorInvalidFormat => {
            std.debug.print("[ERROR] Error when parsing content '{s}'. Color code must match template '#<3 or 6 hex digits>'\n", .{str});
            return;
        }
	};

	// Generate shades
	const color_shades = base_color.getShades(cli_args.mode, cli_args.shades) catch {
	       std.debug.print("[ERROR] Error when generating shades", .{});
		   return;
	};

	// Output shades
	const stdout = std.io.getStdOut().writer();
	for (color_shades, 0..) |shade, idx| {
	   stdout.print("{s}{d};{d};{d}m{d}) Shade {s} : ████████\x1b[0m\n", .{PREFIX_APPLY_HEX, shade.red, shade.green, shade.blue, idx + 1, shade.hexString()}) catch {};
	}
}
