const std = @import("std");

// Errors while parsing
pub const ParseErr = error{
    ParseErrorInvalidLength,
    ParseErrorInvalidFormat,
};

// Errors while creating shades
pub const ShadeErr = error{
    ShadeErrorInvalidMode,
};

// Enum for mode of shading
pub const ColorflowMixMode = enum {
    invalid,
    darken,
    lighten,
    equal,
    pub fn fromString(str: []const u8) ColorflowMixMode {
        if (std.mem.eql(u8, str, "darken")) {
            return .darken;
        } else if (std.mem.eql(u8, str, "lighten")) {
            return .lighten;
        } else if (std.mem.eql(u8, str, "equal")) {
            return .equal;
        } else {
            return .invalid;
        }
    }
};

// Color struct
pub const Color = struct {
    red: u8 = 0,
    green: u8 = 0,
    blue: u8 = 0,
    // Custom format function
    pub fn format(self: Color, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype,) !void {
        _ = fmt;
        _ = options;

        try writer.writeAll("Color ");
        try writer.print("{{ r: {d}", .{self.red});
        try writer.print(", g: {d}", .{self.green});
        try writer.print(", b: {d}", .{self.blue});
        try writer.writeAll(" }");
    }
    // Expands the rgb code of a color when the short form is used
    fn expandShortForm(self: *[7] u8) void {
        var i: u8 = 3;
        while (i > 0) {
            self[i*2] = self[i];
            self[(i*2)-1] = self[i];
            i = i - 1;
        }
        return;
    }
    // Returns the hex code string of a color
    pub fn hexString(self: Color) [7]u8 {
        const charset = "0123456789abcdef";
        var str_buffer: [7] u8 = undefined;

        str_buffer[0] = '#';
        str_buffer[1] = charset[self.red >> 4];
        str_buffer[2] = charset[self.red & 15];
        str_buffer[3] = charset[self.green >> 4];
        str_buffer[4] = charset[self.green & 15];
        str_buffer[5] = charset[self.blue >> 4];
        str_buffer[6] = charset[self.blue & 15];

        return str_buffer;
    }
    // Reads a string and parses a valid color or fails with any ParseErr
    pub fn parse(str: []const u8) ParseErr!Color {
        var color: Color = undefined;
        var content: [7]u8 = undefined;
        var hexValues: [6]u8 = undefined;

        // Convert to lowercase
        _ = std.ascii.lowerString(&content, str);
        // Check length and format
        if (str[0] != '#') {
            return ParseErr.ParseErrorInvalidFormat;
        } else if (str.len != 4 and str.len != 7) {
            return ParseErr.ParseErrorInvalidLength;
        }

        // Check for short form and expand
        if (str.len == 4) {
            expandShortForm(&content);
        }

        // Only get hex values
        const hexContent = content[1..];
        // Check for non-hex characters
        for (hexContent, 0..) |character, idx| {
            const hex = std.fmt.charToDigit(character, 16) catch {
                return ParseErr.ParseErrorInvalidFormat;
            };
            // Extract hex values
            hexValues[idx] = hex;
        }

        // Calculate hex values and store to struct
        color.red = hexValues[0] * 16 + hexValues[1];
        color.green = hexValues[2] * 16 + hexValues[3];
        color.blue = hexValues[4] * 16 + hexValues[5];

        // Return struct
        return color;
    }
    // Get shades of a color based on the used mode
    // Returns slice of colors or error
    pub fn getShades(self: *const Color, mode: ColorflowMixMode, shades: u8) ![]Color {
        // Get allocator used for dynamics slice sizing
        var allocator = std.heap.page_allocator;
        // Allocate slice for returned colors
        const color_shades: []Color = try allocator.alloc(Color, shades);

        // Switch on mode
        switch (mode) {
            .darken => {
                // Get interval used for darker shades
                const shade_interval_red: u8 = self.red / shades;
                const shade_interval_green: u8 = self.green / shades;
                const shade_interval_blue: u8 = self.blue / shades;

                // Iterate over shade count and add shaded color to slice
                var iter: u8 = 0;
                while (iter < shades) {
                    const shaded_red: u8 = self.red - (shade_interval_red*iter);
                    const shaded_green: u8 = self.green - (shade_interval_green*iter);
                    const shaded_blue: u8 = self.blue - (shade_interval_blue*iter);
                    const shade = Color{ .red=(shaded_red), .green=(shaded_green), .blue=(shaded_blue)};
                    color_shades[iter] = shade;
                    iter += 1;
                }

                // Return shades
                return color_shades;
            },
            .lighten => {
                // Get interval used for darker shades
                const shade_interval_red: u8 = (255 - self.red) / shades;
                const shade_interval_green: u8 = (255 - self.green) / shades;
                const shade_interval_blue: u8 = (255 - self.blue) / shades;

                // Iterate over shade count and add shaded color to slice
                var iter: u8 = 0;
                while (iter < shades) {
                    const shaded_red: u8 = self.red + (shade_interval_red*iter);
                    const shaded_green: u8 = self.green + (shade_interval_green*iter);
                    const shaded_blue: u8 = self.blue + (shade_interval_blue*iter);
                    const shade = Color{ .red=(shaded_red), .green=(shaded_green), .blue=(shaded_blue)};
                    color_shades[iter] = shade;
                    iter += 1;
                }

                // Return shades
                return color_shades;
            },
            .equal => {
                // Calculate equal shade count, i.e. amount of lightened and darkened shades in comparison to base_color
                const equal_shades_count = shades / 2;

                // Use recursive call to generate lightened and darkened shades
                const darker_shades = try getShades(self, ColorflowMixMode.darken, equal_shades_count + 1);
                const lighter_shades = try getShades(self, ColorflowMixMode.lighten, if (shades % 2 == 0) equal_shades_count else equal_shades_count + 1);

                var iter: u8 = 0;
                // Copy darkened shades to output slice
                while (iter < equal_shades_count) {
                    color_shades[iter] = darker_shades[(darker_shades.len - 1) - iter];
                    iter += 1;
                }
                var light_shade_iter: u8 = 0;
                // Copy lightened shades to output slice
                while (iter < shades) {
                    color_shades[iter] = lighter_shades[light_shade_iter];
                    iter += 1;
                    light_shade_iter += 1;
                }

                // Return shades
                return color_shades;
            },
            .invalid => {
                // Return error on invalid mode selection
                return ShadeErr.ShadeErrorInvalidMode;
            }
        }
    }
};
